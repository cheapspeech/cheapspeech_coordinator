import axios, { AxiosResponse } from "axios";
import ManifestResource from "../manifest_resource";

const secondsBetweenHeartbeatIterations = 4 * 1000
const getHeartbeatTimeout = 5 * 1000
const maximumFailedCalls = 3

export default class ShardHeartbeatTask {

    static async startHeartbeatLoop() {
        var loop = async () => {
            await ShardHeartbeatTask.heartbeatLoop()
            setTimeout(loop, secondsBetweenHeartbeatIterations)
        }
        setTimeout(loop, secondsBetweenHeartbeatIterations)
    }

    static async heartbeatLoop() {
        console.log(ManifestResource.manifest)
        var newActiveHashSchemas: Array<number> = []

        for (var shard of ManifestResource.manifest.fileShardAddresses) {
            try {
                var res = await this.getHeartbeat(shard.address, shard.port)
                if (res) {
                    ShardHeartbeatTask.updateManifestWithActiveHashSchemas(res, newActiveHashSchemas)
                    shard.failedCalls = 0
                }
                else {
                    shard.failedCalls++
                }
            }
            catch (e) {
                shard.failedCalls++
            }
        }

        ManifestResource.manifest.fileShardAddresses = ManifestResource.manifest.fileShardAddresses.filter((elem) => {
            return elem.failedCalls <= maximumFailedCalls
        })

        ManifestResource.manifest.activeHashSchemas = newActiveHashSchemas
    }

    static async getHeartbeat(address: string, port: string): Promise<AxiosResponse | null> {
        try {
            var url: string = "http://" + address + ":" + port + "/heartbeat"
            var response: AxiosResponse = await axios.post(url,
                {
                    timeout: getHeartbeatTimeout
                },
            )
            return response.status == 200 ? response : null
        }
        catch (e) {
            console.log("Failed to connect to shard on " + address + ":" + port)
            return null;
        }
    }

    static updateManifestWithActiveHashSchemas(res: AxiosResponse, hashes: Array<number>) {
        var shardSchemas = res.data.activeHashSchemas
        for (var schema of shardSchemas) {
            if (!hashes.includes(schema)) {
                hashes.push(schema)
            }
        }
    }
}