import axios, { AxiosResponse } from "axios";
import ManifestResource from "../manifest_resource";

const secondsBetweenHeartbeatIterations = 4 * 1000
const getHeartbeatTimeout = 5 * 1000
const maximumFailedCalls = 3

export default class RestHeartbeatTask {

    static async startHeartbeatLoop() {
        var loop = async () => {
            await RestHeartbeatTask.heartbeatLoop()
            setTimeout(loop, secondsBetweenHeartbeatIterations)
        }
        setTimeout(loop, secondsBetweenHeartbeatIterations)
    }

    static async heartbeatLoop() {
        for (var container of ManifestResource.manifest.restAddresses) {
            try {
                var res = await this.getHeartbeat(container.address, container.port)
                if (res) {
                    container.failedCalls = 0
                }
                else {
                    container.failedCalls++
                }
            }
            catch (e) {
                container.failedCalls++
            }
        }

        ManifestResource.manifest.restAddresses = ManifestResource.manifest.restAddresses.filter((elem) => {
            return elem.failedCalls <= maximumFailedCalls
        })
    }

    static async getHeartbeat(address: string, port: string): Promise<AxiosResponse | null> {
        try {
            var url: string = "http://" + address + ":" + port + "/heartbeat"
            var response: AxiosResponse = await axios.post(url,
                {
                    timeout: getHeartbeatTimeout
                },
            )
            return response.status == 200 ? response : null
        }
        catch (e) {
            console.log("Failed to connect to container on " + address + ":" + port)
            return null;
        }
    }
}