import { Request, Response } from "express"
import { Manifest } from "cheapspeech-shared"
import ShardHeartbeatTask from "./background_work/shard_heartbeat_task"
import AppStatus from "./persistence/app_status"
import RestHeartbeatTask from "./background_work/rest_heartbeat_task"

export default class ManifestResource {
    static manifest: Manifest = new Manifest()

    static async wrapCall(callback: Function, res: Response, next: Function) {
        try {
            await callback()
            next()
        }
        catch (err) {
            console.log(err)
            ManifestResource.errorResponse(res, err, next)
        }
    }

    static errorResponse(res: Response, err: any, next: Function) {
        if (err instanceof ManifestResourceError) {
            res.status(err.code).type("application/json").send(err.msg)
        }
        else {
            next(err)
        }
    }

    static registerFileShard(req: Request, res: Response, next: Function) {
        ManifestResource.wrapCall(async () => {
            var register: Register = new Register(req, res, next)
            await register.registerFileShard()
        }, res, next)
    }

    static registerRestContainer(req: Request, res: Response, next: Function) {
        ManifestResource.wrapCall(async () => {
            var register: Register = new Register(req, res, next)
            await register.registerRestContainer()
        }, res, next)
    }

    static getManifest(req: Request, res: Response, next: Function) {
        res.status(200).type("application/json").send(ManifestResource.manifest.toJson())
    }

    static setRehashTarget(req: Request, res: Response, next: Function) {
        ManifestResource.wrapCall(async () => {
            if (!req.body.hasOwnProperty("hashTarget")) {
                res.status(422).type("application/json").send(JSON.stringify({ msg: "Missing hashTarget parameter." }))
            }
            else {
                ManifestResource.manifest.targetHashSchema = req.body.hashTarget
                AppStatus.writeAppStatus()
                res.sendStatus(200)
            }

            next()
            return
        }, res, next)
    }
}

class Register {
    req: Request;
    res: Response;
    next: Function;

    constructor(req: Request, res: Response, next: Function) {
        this.req = req
        this.res = res
        this.next = next
    }

    async registerFileShard() {
        this.validateCall(true)
        var address = this.req.body.address
        var port = this.req.body.port
        var shardNumber = this.req.body.shardNumber

        var verified = await ShardHeartbeatTask.getHeartbeat(address, port)
        if (!verified) {
            throw new ManifestResourceError(JSON.stringify({
                msg: "Could not contact heartbeat service on shard."
            }), 400)
        }
        else if (this.shardAlreadyRegistered(address, port, shardNumber)) {
            throw new ManifestResourceError(JSON.stringify({
                msg: "Shard already registered."
            }), 400)
        }
        else {
            ManifestResource.manifest.addFileShard(address, port, shardNumber)
            this.next()
        }
    }

    async registerRestContainer() {
        this.validateCall(false)
        var address = this.req.body.address
        var port = this.req.body.port
        var verified = await RestHeartbeatTask.getHeartbeat(address, port)
        if (!verified) {
            throw new ManifestResourceError(JSON.stringify({
                msg: "Could not contact heartbeat service on shard."
            }), 400)
        }
        else if (this.restContainerAlreadyRegistered(address, port)) {
            throw new ManifestResourceError(JSON.stringify({
                msg: "Container already registered."
            }), 400)
        }
        else {
            ManifestResource.manifest.addRestContainer(address, port)
            this.next()
        }
    }

    validateCall(isShard: boolean) {
        if (!this.req.body.hasOwnProperty("address")) {
            throw new ManifestResourceError(JSON.stringify({
                msg: "Missing address."
            }), 422)
        }
        else if (!this.req.body.hasOwnProperty("port")) {
            throw new ManifestResourceError(JSON.stringify({
                msg: "Missing port."
            }), 422)
        }
        else if (isShard && !this.req.body.hasOwnProperty("shardNumber")) {
            throw new ManifestResourceError(JSON.stringify({
                msg: "Missing shardNumber."
            }), 422)
        }
        else if (isShard && this.req.body.shardNumber == -1) {
            throw new ManifestResourceError(JSON.stringify({
                msg: "shardNumber cannot be -1, did you forget to initialize the shardNumber variable?"
            }), 422)
        }

    }

    shardAlreadyRegistered(address: string, port: string, shardNumber: number): boolean {
        return ManifestResource.manifest.fileShardAddresses.some((elem) => {
            if (elem.address == address && elem.port == port) {
                return true;
            }
            else if (elem.shardNumber == shardNumber) {
                return true;
            }
            else {
                return false;
            }
        })
    }

    restContainerAlreadyRegistered(address: string, port: string): boolean {
        return ManifestResource.manifest.fileShardAddresses.some((elem) => {
            if (elem.address == address && elem.port == port) {
                return true;
            }
            else {
                return false;
            }
        })
    }
}

export class ManifestResourceError extends Error {
    msg: string = ""
    code: number = 500

    constructor(msg: string, code: number) {
        super(msg)
        this.msg = msg
        this.code = code
    }
}