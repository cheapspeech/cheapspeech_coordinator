import { Request, Response } from "express"
import RuntimeConfigs from "./persistence/runtime_configs"


export default class RequestValidation {
    static isValid(req: Request, res: Response, next: Function) {
        if (!req.headers.hasOwnProperty("access-token")) {
            res.sendStatus(401)
        }
        else if (req.headers["access-token"] != RuntimeConfigs.accessToken) {
            res.sendStatus(401)
        }
        else {
            next()
        }
    }
}