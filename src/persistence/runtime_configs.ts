import fs from "fs/promises"

export default class RuntimeConfigs {
    static port: string = "";
    static accessToken: string = "";

    static async readConfigs() {
        try {
            var configFile = await fs.readFile("./conf/app_config.json")
        }
        catch (err) {
            throw new Error("Could not read config file ./conf/app_config.json")

        }

        var configFileJson = JSON.parse(configFile.toString())
        if (!configFileJson.hasOwnProperty("port")) {
            throw new Error("Config file missing port attribute")
        }
        else if (!configFileJson.hasOwnProperty("accessToken")) {
            throw new Error("Config file missing accessToken attribute")
        }

        RuntimeConfigs.port = configFileJson.port
        RuntimeConfigs.accessToken = configFileJson.accessToken
    }
}