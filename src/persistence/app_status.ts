import fs from "fs/promises"
import ManifestResource, { ManifestResourceError } from "../manifest_resource"

export default class AppStatus {

    static async readAppStatus() {
        try {
            var configFile = await fs.readFile("./conf/app_status.json")
            var appStatus = JSON.parse(configFile.toString())
            ManifestResource.manifest.targetHashSchema = appStatus.targetHashSchema
        }
        catch (err) {
            console.log("Unable to find app status file, refusing to start.")
            process.exit(1)
        }
    }

    static async writeAppStatus() {
        try {
            var targetHashSchema = ManifestResource.manifest.targetHashSchema
            var appStatus = {
                targetHashSchema: targetHashSchema
            }
            var appStatusString = JSON.stringify(appStatus)
            await fs.writeFile("./conf/app_status.json", appStatusString)
        }
        catch (e) {

        }
    }
}