import express from "express";
import fs from "fs";
import ShardHeartbeatTask from "./background_work/shard_heartbeat_task";
import ManifestResource from "./manifest_resource";
import RequestValidation from "./request_validation";
import RuntimeConfigs from "./persistence/runtime_configs";
import AppStatus from "./persistence/app_status";
import RestHeartbeatTask from "./background_work/rest_heartbeat_task";

class AppGlobal {
  static app = express();

  static async initialize() {
    await RuntimeConfigs.readConfigs()
    await AppStatus.readAppStatus()

    ShardHeartbeatTask.startHeartbeatLoop()
    RestHeartbeatTask.startHeartbeatLoop()
    AppGlobal.app.use(express.json())
    AppGlobal.createRoutes()

    AppGlobal.app.use((error: Error, req: any, res: any, next: any) => {
      fs.appendFile("log.txt", error.name, () => { });
      fs.appendFile("log.txt", error.message, () => { });
      fs.appendFile("log.txt", error.stack + "\n\n" as string, () => { });
      res.status(500).type("application/json").send(JSON.stringify({
        msg: "An unidentified error occured."
      }))
    })

    AppGlobal.app.listen(RuntimeConfigs.port);
  }

  static createRoutes() {
    AppGlobal.app.post("/register/fileShard", [RequestValidation.isValid, ManifestResource.registerFileShard, ManifestResource.getManifest])
    AppGlobal.app.post("/register/restContainer", [RequestValidation.isValid, ManifestResource.registerRestContainer, ManifestResource.getManifest])
    AppGlobal.app.post("/manifest/setHashTarget", [RequestValidation.isValid, ManifestResource.setRehashTarget])
    AppGlobal.app.post("/manifest/get", [RequestValidation.isValid, ManifestResource.getManifest])
    AppGlobal.app.post("/manifest/rehashComplete", () => { })
  }
}

AppGlobal.initialize()